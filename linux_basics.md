# Linux alapok

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Linux alapok](#linux-alapok)
	- [Operációs rendszerek – alapok](#opercis-rendszerek-alapok)
	- [Szerver elérés](#szerver-elrs)
		- [Konzol](#konzol)
		- [SSH](#ssh)
	- [Segítség - manual](#segtsg-manual)
	- [Fájl kezelés](#fjl-kezels)
	- [Memória kezelés](#memria-kezels)
	- [Processz kezelés](#processz-kezels)
	- [Tárhely kezelés](#trhely-kezels)
	- [Hálózat kezelés](#hlzat-kezels)
	- [Systemd](#systemd)
	- [I/O átirányítás](#io-tirnyts)
	- [Felhasználó kezelés](#felhasznl-kezels)
	- [Scripting](#scripting)

<!-- /TOC -->

## Operációs rendszerek – alapok

Az operációs rendszerek feladata, hogy magát a hardvert kezeljék és környezetet biztosítsanak a operációs rendszeren futattott alkalmazásoknak. A hardver kezelése a fizikai alkatrészek által biztosított erőforrások kezelését jelenti. Legyen ez alkatrész egy memória modul, vagy valamilyen háttértár. Például merevlemez vagy ssd. Ezekhez kell az eszközök drivere.
Az operációs rendszer gazdálkodik az olyan erőforrásokkal mint a processzor idő, memória területek, háttértár területek, vagy hálózati elérés. Ha valamelyik alkalmazás ezekből szeretne, az operációs rendszertől tudja elkérni.
Ezek az operációs fő feladatai, amiket többnyire a rendszer magja, a kernel lát el. Ez a felhasználók számára nem elérhető, ha a rendszerrel kell kommunikálni, azt egy rendszer héjon, shell-en keresztül tudjuk megtenni. Ezek a shell-ek lehetnek szöveges vagy grafikus felületek. A grafikus felületekre példa a Windows-ok vagy Linuxok grafikus, ablakozó felülete, míg a szöveges felületre példa a Powershell és a Bourne Again Shell avagy Bash.

## Szerver elérés

Egy Linux rendszer elérésénél adminisztratív célokra általában valamilyen szöveg alapú elérés a leghatékonyabb. A korai rendszereknél kialakult a Command Line Interface avagy CLI. Ezen keresztül könnyen automatizálhatók bizonyos feladatok.
GUI + Terminál

### Konzol

Fizikai hozzáférés esetén a GUI mellett konzol elérés is található a CTRL+ALT+fX gombok lenyomásával. Ez az adott rendszer konfigurációjától függ, hogy X eleme [1,2,3,4,5,6,7]

### SSH

Ha tudjuk egy rendszer IP címét vagy domain nevét és telepítettek rá SSH kiszolgálót, akkor távolról is elérhető a megfelelő porton.

## Segítség - manual
Man, info

## Fájl kezelés

ls, pwd, tree

cd, mkdir, rmdir

touch, stat, nano, vi/vim

chmod, chown

## Memória kezelés

free, swapon/swapoff

## Processz kezelés

ps, top/htop, sudo, kill, nice

## Tárhely kezelés

Du, df. Ls -ls


lsblk, blkid

mount, umount

parted, mkfs

## Hálózat kezelés

netstat/ss(socket stat), ntop, nload

ip a, ip r (ifconfig, route)

/etc/network/interfaces, netplan, NetworkManager

ping, traceroute, tcpdump

iptables, ufw, firewall-cmd

## Systemd

Systemctl, resolvctl, timedatectl

## I/O átirányítás

std I/O:
- stdIN: filedescriptor 0
- stdOUT: filedescriptor 1
- stdERR: filedescriptor 2

echo "sth" > file1.txt

## Felhasználó kezelés

useradd, adduser, deluser

passwd, chage

## Scripting

\#!/bin/env bash
